
var open = false;

var openButton = helper.id('open');

openButton.addEventListener('click',navToggle);

function navToggle(){
 
     
     var nav  =  helper.id('nav');
     var section  =  helper.id('section');

    if(open){
       //closed
       nav.style.left = "-250px";
       section.style.marginLeft = "0px";
       open = false;
    } else{
        // open
        nav.style.left = "0px";
        section.style.marginLeft = "250px";
        open = true;
    }
     

}