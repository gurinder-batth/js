var validations = {
    run(){
           for(rule in this.rules){
                    var ele  = document.getElementById(rule);
                    var rule = this.rules[rule];
                    this.error[ele.id] = [];
                    console.log(this.error[ele.id])
                    for(method in  rule) {
                        // console.log(rule[method])
                           var method = rule[method].split(":");
                            if(method.length == 1){
                                this[method[0]](ele);
                            } else{
                                this[method[0]](ele,method[1]);
                            }
                    }  
           }
           this.displayError();
        //    console.log(this.error)
    },
    rules:null,
    error:[],
    displayError(){
        for(rule in this.rules){
            if(this.error[rule].length > 0){
                 document.getElementById(rule+"_error").innerHTML = this.error[rule][0];
            } else{
                document.getElementById(rule+"_error").innerHTML = '';
            }
        }
    },
    required(ele){
          if(ele.value == ''){
              this.error[ele.id].push(ele.id + " Filed Required");
          }
    },
    min(ele,length){
        if(ele.value.length < length){
            this.error[ele.id].push(ele.id + " minimum length should be " + length);
        }         
    },
    max(ele,length){
        if(ele.value.length > length){
           this.error[ele.id].push(ele.id + " maximum length should be " + length);
        }
    }
}