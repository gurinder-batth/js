var validations = {

                  rules:[], //Rules Pass By Developer
                  errors:{}, //Store the errors
                  
                  //Run Method Invoke the Validations Method Which is Selected by Developer on particular Field 
                  run(){
                    
                       
                          for(field in this.rules){
                                
                                //    Field Contain The Object
                                //  console.log(field + " -> " +this.rules[field])
                                // console.log(field)
                                this.errors[field] = [];
                                var ele = document.getElementById(field);
                                 for(i = 0 ; i < this.rules[field].length ; i++){
                                                 //   console.log(this.rules[field][i])
                                                   //   this.rules  = {
                                                    //             firstname:['required'] ,
                                                    //             lastname:['required'] ,
                                                    //             fathername:['required'] ,
                                                    // }

                                                    // field  = firstname | lastname | fathername

                                                    // i = position of element  eg: 0,1,2,3 

                                                    //  this.rules[field][i] = required
                                                    // this.required(ele); Hard Code Call Method
                                          
                                                  //  this[this.rules[field][i]](ele); //Dynamically Called

                                                  var method = this.rules[field][i].split(":");
                                                //   if method Array Length is 2 It means method has argument  otherwise it recieve not argument
                                            //  method[0]  = "Value represent the Method Name"
                                            //  method[1]  = "Value represent the Argument"
                                                if(method.length == 2){
                                                            this[method[0]](ele,method[1]);  //Invoke Developer Argument Method
                                                } else{
                                                    this[method[0]](ele);  //Invoke With Argument method
                                                }
                                 }
                          }
                               
                        console.log(this.errors);
                        this.displayError();

                  }       ,

                  displayError(){
                             
                    // Display Error Show the Error in HTML Form And Suppose to Prefix is _error after ID of Input
                        
                       for(field in this.errors){
                                 var error_ele  = document.getElementById(field+"_error");
                                 if(this.errors[field].length > 0){
                                     error_ele.innerHTML = this.errors[field][0];
                                 } else{
                                    error_ele.innerHTML = "";
                                 }
                       }
                 
                  },

                //   VALIDATIONS METHODS START FROM HERE
                required(ele){
                     if(ele.value == ""){
                         this.errors[ele.id].push(ele.id + " is required")
                     }
                } ,
                min(ele,length){
                     if(ele.value.length < length){
                         this.errors[ele.id].push(ele.id + " minimum length should be "+ length);
                     }
                } 

}