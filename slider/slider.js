var slider = {
          
        //  image-slider element object
        img:null ,
        position:0,
        images:[] ,
        timer:2,

        start(){

            this.defaultImage();


            setInterval( () => {
                
                this.changeImage();
                

            } , this.timer * 1000 )

        },

        // Change Images
        changeImage(){
              this.position++;
              this.img.src = this.images[this.position];
              if(this.images.length - 1 == this.position) {  this.position = -1  }
        } ,
        // 
        defaultImage(){
               this.img.src = this.images[this.position];
        }

}